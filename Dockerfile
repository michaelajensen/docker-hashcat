##################################  Notes  ##################################
# to build:
#   docker build -t hashcat .
#
# to run:
#   docker run --gpus all hashcat nvidia-smi
#
# to run with a mounted work directory:
#   docker run -ti -v /path/to/local/workdir:/workdir --gpus all hashcat
#
#############################################################################

FROM nvidia/cuda:10.2-devel-ubuntu18.04

MAINTAINER Michael Jensen version: 0.9

RUN apt-get update && apt-get install -y build-essential git
RUN apt-get update && apt-get install -y --no-install-recommends \
        ocl-icd-opencl-dev clinfo pocl-opencl-icd vim tmux
RUN apt-get clean && rm -rf /var/lib/apt/lists/*

RUN cd / && git clone https://github.com/hashcat/hashcat.git
RUN cd /hashcat && make

RUN cd / && git clone https://github.com/hashcat/hashcat-utils.git
RUN cd /hashcat-utils/src && make

RUN mkdir /workdir

RUN mkdir -p /etc/OpenCL/vendors && \
    echo "libnvidia-opencl.so.1" > /etc/OpenCL/vendors/nvidia.icd



CMD ["/bin/bash"]


# nvidia-container-runtime
ENV NVIDIA_VISIBLE_DEVICES all
ENV NVIDIA_DRIVER_CAPABILITIES compute,utility
